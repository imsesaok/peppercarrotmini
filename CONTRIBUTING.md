# Contributing translations or corrections

**First of all!**  
I advise to read the [CONTRIBUTING.md](https://framagit.org/peppercarrot/webcomics/blob/master/CONTRIBUTING.md) from the main repo, because it is **exactly** the same method, and this file is very well done :o)
Here, I only try to put the main points developed in the main file, adapted to the case of Pepper and Carrot MINI.

## What you need :

- **[Inkscape](https://inkscape.org/en/ "Inkscape")**: the free/libre and open-source vector drawing software.
- **[The source repository](https://framagit.org/Nartance/peppercarrotmini/ "Git repository")**: it contains all translations of Pepper and Carrot MINI (`lang/` folder, one folder by language). You can clone it if you are familiar with Git or [directly download it](https://framagit.org/Nartance/peppercarrotmini/repository/master/archive.zip "directly download it").
- **[The fonts](https://framagit.org/Nartance/peppercarrotmini/tree/master/tools/fonts)**: For installing instructions, please refer to the README.md files in it.

## Problem? Question? How to know if someone already work on a specific language?

[The online bug-tracker](https://framagit.org/Nartance/peppercarrotmini/issues) on Framagit contains all discussions related to translation. You have a question? Open an issue. You want a place to exchange with other translators of your language? Issue. Issues are THE way of communicate. You can [open a new issue](https://framagit.org/Nartance/peppercarrotmini/issues/new?issue) or looking for one already opened for the episode you want to translate. You can add a prefix before to indicate if it's a question (like _[Question]_) or a main discussion about translation for one language (like _[WIP][FR] Episode XX_). Don't forget to think about other translators who want to help you in the language you are translating :o)

  
## Create your folder

In the source, open the folder `lang/`. Duplicate in it an existing language folder and rename it to the language you want to translate. Be careful: in a language subfolder, there are ALL episodes. If you only want to translate one by one, I suggest to only take the .svg of the episode you want from another language folder, and put it in your new language fodler. On Pepper&Carrot (so on Pepper and Carrot MINI :o) ), we use two letters only for the country based on [ISO language codes.](http://www.w3schools.com/tags/ref_language_codes.asp "ISO language codes."). But if your target lang doesn't have any two letter ISO code, we can invent one. 
  
  
## Start translating

### Working on your own

Inside the folder you just created, just edit all the SVG files with Inkscape. Translate the content of speech-bubbles, change sound the sounds effect, resize speech-bubble to look good with your new content. Feel free to be the art-director of your translation, and resize, arrange the layout to your taste. Don't forget to your name in the AUTHOR.md file ( at root of the repository ) as well.
You can do your own render with Inkscape (Maj+Ctrl+E, or File > Export to PNG). The path is in the `export/` folder (`Export As`... in the render panel), on the subfolder of the episode you want to render. Just pick the filename of one of other rendered episode and change just the ISO code of the language by yours (the name of the folder where you are working). For French and English, I render the Page with 300dpi.
**Of course, it's not an obligation to do the render by yourself.** I can do it if I see a new language on the repo, or by mail.

### Working with others

If you’d like to work with other people on [the Framagit repo](https://framagit.org/Nartance/peppercarrotmini/), it’s easy to register on Framagit.

## Send your work

There are three ways of sending us your work.

- **Using Git**: You can make [merge requests](https://docs.gitlab.com/ee/gitlab-basics/add-merge-request.html "a merge request") on this repository. 
I consider that if you translate Pepper and Carrot MINI, your a translator of Pepper&Carrot. There are groups of translators on the main website, and I share this repo with all of them. If you want to be part of them, just let me know your Framagit nickname, and I'll ask to add you on the main [member list](https://framagit.org/groups/peppercarrot/-/group_members) with all the permissions to edit and push the files.
- **Using the Bug-tracker**: Pack your language folder and your render if you have done it into a ZIP file; it's very lightweight (no need of the whole repository) and attach your translation to a [new issue](https://framagit.org/Nartance/peppercarrotmini/webcomics/issues/new?issue) on the bug-tracker. Don't forget to tell how I should credit you on AUTHORS.md file. If you want me to do the render, you can tell it in the bug-tracker.
- **By email**:  Pack your language folder and your render if you have done it into a ZIP file; it's very lightweight (no need of the whole repository) and attach it to an email to [nicolas.artance@gmail.com](mailto:nicolas.artance@gmail.com). Don't forget to tell how I should credit you on [AUTHORS.md](https://framagit.org/Nartance/peppercarrotmini/blob/master/AUTHORS.md) file. If you want me to do the render, you can tell it in the mail.

**And that's all :o)** All SVG files added to the the source repository will be rendered and posted online on [the website](https://www.peppercarrot.com/?static11/community-webcomics&page=Pepper-and-Carrot-Mini_by_Nartance/)!

## FAQ on the main repo

If you have a question, I suggest you to read the FAQ part of the [CONTRIBUTING.md](https://framagit.org/peppercarrot/webcomics/blob/master/CONTRIBUTING.md) file of the main repo. All anwsers can be applied in this case (except answers based on others parts of the website. For example, there is no overview of all translated languages for Pepper and Carrot MINI on the website).



## License: terms and conditions

The conditions are exactly the same than the main repo. I put it entirely here (and adapt it to my case) to be as clearest as possible.

By submitting any content to the Pepper and Carrot MINI [repository](https://framagit.org/Nartance/peppercarrotmini), such as new translations, improvements to translations, issues, or other content, (“Your Content”), you accept that you release it under the [Creative Commons Attribution 4.0 International](https://creativecommons.org/licenses/by/4.0/) License (“CC BY 4.0”). You acknowledge that you understand this license and you permit everyone to use Your Content under this license. You warrant that you have the right to grant this permission and you understand that under the terms of the CC BY 4.0 license you cannot later revoke it.

You are of course still free to make derivatives and translations of Pepper and Carrot MINI and distribute them under other licenses, so long as you comply with the applicable licenses. But if you wish to have them included in this repository or on [the official website](https://www.peppercarrot.com/?static11/community-webcomics&page=Pepper-and-Carrot-Mini_by_Nartance/), you must release them under CC BY 4.0.

(This does not apply to files in the `tools/fonts/` directory, which are released under their own separate license agreements.)

All the attributions are listed on the [AUTHORS.md](https://framagit.org/Nartance/peppercarrotmini/blob/master/AUTHORS.md) files at the root of the repository.
