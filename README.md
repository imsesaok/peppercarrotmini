# Pepper and Carrot MINI repo

![cover image](./png/_cover/pepper_mini_vs_real_pepper_thumb.png)

This repository contains all the files necessary to translate the derivative webcomics [Pepper and Carrot MINI](https://www.peppercarrot.com/?static11/community-webcomics&page=Pepper-and-Carrot-Mini_by_Nartance/), based on [Pepper&Carrot](http://www.peppercarrot.com) by David Revoy.

## Main repo

You can find the repo of the main webcomics [here](https://framagit.org/peppercarrot). I'm trying to copy the way of translating to simplify the work of translators. The regulars won't be lost, and the new ones could find a reliable documentation on the main repo. 

## How to translate

As said above, it's the same system than the main repo. You can find a documentation [here](https://framagit.org/peppercarrot/webcomics/blob/master/CONTRIBUTING.md), to see how it is done for the main webcomics. There are some changes in the folders here, explained in the [CONTRIBUTING.md](CONTRIBUTING.md) file of this repo.

In a nutshell: you need to install Inkscape and fonts (see the subfolder `tools/fonts` for more details), clone the repo, and copy/paste a folder in the `lang/` repo, give the [ISO name](https://framagit.org/peppercarrot/webcomics/blob/master/lang-ISO.json) of your language, and edit all the files.

You can also add the little description for the website in the `about/` folder.

## References

Pepper and Carrot MINI is based on Pepper&Carrot. You can see the translation file for all elements of the main universe [here](https://framagit.org/peppercarrot/webcomics/blob/master/translation-names-references.fods). If your are curious, you can also visit the [wiki](https://www.peppercarrot.com/static8/wiki) of the main website to learn more about Pepper&Carrot universe and... maybe contribute ? :o)

## Credits

Read [AUTHORS.md](AUTHORS.md) in the root of this repository for the full attribution.

## Sources artworks and rendering

The artwork in the `png` subfolders are the base of the translation. Do not edit or propose a commit for the artworks. 
You can do the render of your language by yourself. The folder `exports/` has a folder by episode, with all translations rendered. If you want to add yours, you just have to export via Inkscape into this folder.


## Fonts

You can find the infos about fonts used in Pepper and Carrot MINI into the subfolder `tools/fonts` to learn more about the fonts, and their licenses.

## License

Content in this repository is licensed under the [Creative Commons Attribution 4.0 International](https://creativecommons.org/licenses/by/4.0/) License, except for the fonts in the `tool/fonts/` directory, which are released under their own separate license agreements.

By submitting content to this repository, one agrees to the [contributor's terms](CONTRIBUTING.md).
