Pepper and Carrot MINI contributors
==========================

## Main webcomics Author

* [David Revoy](https://framagit.org/Deevad), founder of [www.peppercarrot.com](https://www.peppercarrot.com/)

## Artwork for Pepper and Carrot MINI

* [Nicolas Artance](https://framagit.org/Nartance)

## Translations

* English: [Nicolas Artance](https://framagit.org/Nartance)

* French: [Nicolas Artance](https://framagit.org/Nartance)

